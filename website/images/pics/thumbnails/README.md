Thumbnail versions of images for showing on the actual site

All thumbnails have a width of 750px

To resize:
Open in Paint3D > Canvas > Change width value